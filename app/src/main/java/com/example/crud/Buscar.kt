package com.example.crud

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.crud.databinding.ActivityBuscarBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class Buscar : AppCompatActivity() {
    private lateinit var binding : ActivityBuscarBinding
    private lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBuscarBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSearchData.setOnClickListener {
            val nombre : String = binding.edtNombreD.getText().toString()
            if(nombre.isNotEmpty()){
                readData(nombre)
            }else{
                Toast.makeText(this,"Ingrese un usuario correcto", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun readData(nombre: String) {

        database = FirebaseDatabase.getInstance().getReference("Usuarios")
        database.child(nombre).get().addOnSuccessListener {
            if (it.exists()){
                val nombre = it.child("nombres").value
                val apellido = it.child("apellidos").value
                val edad = it.child("edad").value
                val correo = it.child("correo").value
                val numC = it.child("numC").value

                binding.edtNombreD.text.clear()
                var bundle = Bundle()
                bundle.apply {
                    putString("key_name",nombre.toString())
                    putString("key_apellidos",apellido.toString())
                    putString("key_edad",edad.toString())
                    putString("key_correo",correo.toString())
                    putString("key_numC",numC.toString())
                }

                val intent = Intent(this,Actualizar::class.java).apply{
                    putExtras(bundle)
                }
                startActivity(intent)

            }else{
                Toast.makeText(this,"Usuario no existe", Toast.LENGTH_SHORT).show()
            }
        }.addOnFailureListener {
            Toast.makeText(this,"Fallo!!!", Toast.LENGTH_SHORT).show()
        }
    }
}