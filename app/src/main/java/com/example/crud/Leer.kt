package com.example.crud

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.crud.databinding.ActivityLeerBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class Leer : AppCompatActivity() {
    private lateinit var binding :ActivityLeerBinding
    private lateinit var database : DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLeerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnReadData.setOnClickListener {
            val numC : String = binding.edtNombreD.getText().toString()

            if(numC.isNotEmpty()){
                readData(numC)
            }else{
                Toast.makeText(this,"Ingrese un usuario correcto", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun readData(numC: String) {
        database = FirebaseDatabase.getInstance().getReference("Usuarios")
        database.child(numC).get().addOnSuccessListener {
            if (it.exists()){
                val numC = it.child("numC").value
                val nombres = it.child("nombres").value
                val apellidos = it.child("apellidos").value
                val edad = it.child("edad").value
                val correo = it.child("correo").value

                binding.edtNombreD.text.clear()
                binding.tvNombre.text = nombres.toString()
                binding.tvApellidoP.text = apellidos.toString()
                binding.tvEdad.text = edad.toString()
                binding.tvCorreo.text = correo.toString()
                binding.tvNumC.text = numC.toString()
            }else{
                Toast.makeText(this,"Usuario no existe", Toast.LENGTH_SHORT).show()
            }
        }.addOnFailureListener {
            Toast.makeText(this,"Fallo!!!", Toast.LENGTH_SHORT).show()
        }
    }
}