package com.example.crud

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.crud.databinding.ActivityBorrarBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class Borrar : AppCompatActivity() {
    private lateinit var binding : ActivityBorrarBinding
    private  var reference : DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBorrarBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnEliminar.setOnClickListener {
            val username : String = binding.EdtEliminar.getText().toString()

            if(!username.isEmpty()){
                deleteData(username)
            }else{
                Toast.makeText(this, "Ingrese un username", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun deleteData(username: String) {
        reference = FirebaseDatabase.getInstance().getReference("Usuarios")
        reference!!.child(username).removeValue().addOnCompleteListener {
            if(it.isSuccessful){
                Toast.makeText(this,"Se borro correctamente", Toast.LENGTH_SHORT).show()
                binding.EdtEliminar.text.clear()
            }else{
                Toast.makeText(this,"Ocurrio un fallo", Toast.LENGTH_SHORT).show()
            }
        }

    }
}